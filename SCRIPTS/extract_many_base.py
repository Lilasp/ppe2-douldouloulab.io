# --------------------------------------------------------------------------------------------------------------------------------------------------------------

# Voici le premier script principal du projet. Il rassemble et s'appuie sur tous les autres scripts, sauf run_lda.
# Ce script définit une fonction qui a pour but de filtrer les articles à sélectionner selon des critères donnés en arguments en ligne de commande.
# Cette fonction est un générateur qui yield les articles qui correspondent à certaines dates ou catégories, selon les arguments passés.
# Le contenu des articles est ensuite parsé selon une des trois méthodes : regex, feedparser ou etree ; et seul les titres et descriptions sont retenus ;
# qui font ensuite l'objet d'une analyse morpho-syntaxique.
# Tout cela vient agrémenter l'objet Corpus, qui contient l'objet Article (agrémenté dans la fonction "extract_un_fil"), lui-même contenant l'objet Token (agrémenté dans "analysers")
# Enfin, les fonctions qui définissent le format de sortie sont également appelées dans ce script : l'output peut être en xml, json ou pickle, au choix.

# --------------------------------------------------------------------------------------------------------------------------------------------------------------


# Importation des librairies
from typing import Optional, List, Dict
import argparse
import sys
from datetime import date
from pathlib import Path
import re
from datastructures import Corpus
import extract_un_fil as euf
import analyzers as ana
import format_output as out

# Liste des mois, de janvier à décembre (string).
MONTHS = ["Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug", 
          "Sep",
          "Oct",
          "Nov", 
          "Dec"]

# Création de la liste des jours, une suite d'entiers de 01 à 31 transformé en string.
DAYS = [f"{x:02}" for x in range(1,32)]

# Dictionnaire des catégories qui fait correspondre à son nom le code de la catégorie
CAT_CODES =  {	
            'une' : '0,2-3208,1-0,0', 
            'international' : '0,2-3210,1-0,0',
            'europe' : '0,2-3214,1-0,0', 
            'societe' :	'0,2-3224,1-0,0',
            'idees'	: '0,2-3232,1-0,0',
            'economie':	'0,2-3234,1-0,0',
            'actualite-medias':	'0,2-3236,1-0,0',
            'sport': '0,2-3242,1-0,0',
            'planete': '0,2-3244,1-0,0',
            'culture': '0,2-3246,1-0,0',
            'livres' : '0,2-3260,1-0,0',
            'cinema' : '0,2-3476,1-0,0',
            'voyage' : '0,2-3546,1-0,0',
            'technologies': '0,2-651865,1-0,0',
            'politique' : '0,57-0,64-823353,0',
            'sciences' : 'env_sciences'
            }


# Fonction qui transforme les mois en leur équivalent numérique (Janv == 1)
def convert_month(m:str) -> int:
    return MONTHS.index(m) + 1

# Fonction qui filtre le corpus selon les arguments donnés dans le terminal lors de l'appel de la fonction: les dates et les catégories.
# Cette fonction est un générateur. Elle yield l'article qui corresponde aux filtres donnés, afin que son titre et sa description soit analysés par la suite.
def parcours_path(corpus_dir:Path, categories: Optional[List[str]]=None, start_date: Optional[date]=None, end_date: Optional[date]=None):
    if categories is not None and len(categories) > 0:
        categories = [CAT_CODES[c] for c in categories]
    else:
        categories = CAT_CODES.values() # on prend tout

    for month_dir in corpus_dir.iterdir():
        if month_dir.name not in MONTHS:
            # on ignore les dossiers qui ne sont pas des mois
            continue
        m = convert_month(month_dir.name)
        for day_dir in month_dir.iterdir():
            if day_dir.name not in DAYS:
                # on ignore les dossiers qui ne sont pas des jours
                continue
            d = date.fromisoformat(f"2022-{m:02}-{day_dir.name}")
            if (start_date is None or start_date <= d) and (end_date is None or end_date >= d):
                for time_dir in day_dir.iterdir():
                    if re.match(r"\d\d-\d\d-\d\d", time_dir.name):
                        for fic in time_dir.iterdir():
                            if fic.name.endswith(".xml") and any([c in fic.name for c in categories]):
                                yield(fic)



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # Tous les arguments et les options possibles à renseigner par celui qui appel le script
    parser.add_argument("-m", help="méthode de parsing (et, re ou fp)", default="et")
    parser.add_argument("-s", help="start date (iso format)", default="2022-01-01")
    parser.add_argument("-e", help="end date (iso format)", default="2023-01-01")
    parser.add_argument("-o", help="output file", required=True)
    parser.add_argument("-f", help="output format (xml if not specified)", default="xml")
    parser.add_argument("-a", help="choisir l'analyseur (spacy if not specified)", default="sp")
    parser.add_argument("corpus_dir", help="root dir of the corpus data")
    parser.add_argument("categories",nargs="*", help="catégories à retenir")
    args = parser.parse_args()

    #On décide du parseur de fichier à utlisier
    if args.m == 'et':
        func = euf.extract_et
    elif args.m == 're':
        func = euf.extract_re
    elif args.m == 'fp':
        func = euf.extract_feedparser
    else:
        print("méthode non disponible", file=sys.stderr)
        sys.exit()


    #On vient remplir l'objet Corpus
    corpus = Corpus(args.categories, args.s, args.e, args.corpus_dir, [])
                     
        
    for f in parcours_path(Path(args.corpus_dir), 
            start_date=date.fromisoformat(args.s),
            end_date=date.fromisoformat(args.e),
            categories=args.categories):
        for article in func(f):
            art = None
            # On décide de l'analyseur morpho-syntaxique à utiliser
            if args.a == "sp":
                art = ana.spacy_analyse(article)
            elif args.a == "tk" :
                art = ana.trankit_analyse(article)
            elif args.a == "sz":
                art = ana.stanza_analyse(article)
            else:
                print("analyseur non disponible", file=sys.stderr)
                sys.exit()

            # On ajoute chaque Article (selon les filtres) au Corpus
            corpus.articles.append(art)

            # On décide du format de sortie de l'output
            if args.f == 'pickle' or args.f == 'p':
                #appelle de la fonction
                out.write_pickle(corpus, args.o)
            elif args.f == "json":
                #appelle de la fonction
                out.write_json(corpus, args.o)
            elif args.f == "xml":
                #appelle de la fonction
                out.write_xml(corpus, args.o)
            else:
                print("format de sortie non reconnu", file=sys.stderr)
