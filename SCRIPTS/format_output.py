# --------------------------------------------------------------------------------------------------------------------------------------------------------------

# Ce script définit les fonctions nécessaires pour transformer un objet "Corpus" en un fichier en format :
# 

# 1) json
# 2) pickle
# 3) xml

# La structure de l'objet "Corpus" est définie dans le script "datastructures" ici importée.

# --------------------------------------------------------------------------------------------------------------------------------------------------------------


# Importations des librairies et des objets Corpus, Article et Token (définis dans le script "datastructures"). "etree" est aussi importée pour permettre l'accès aux informations contenues
# dans l'objet "Corpus"
from datastructures import Corpus, Article, Token
from xml.etree import ElementTree as ET
from typing import List
import pickle
import json
from dataclasses import asdict


# Définition de la fonction qui prend en entrée un objet "Corpus" et sort en output un fichier json
def write_json(corpus:Corpus, dest:str):
    with open(dest, 'w') as output_file:
        json.dump(asdict(corpus), output_file, indent = 4, ensure_ascii=False)


# Définition de la fonction qui prend en entrée un objet "Corpus" et sort en output un fichier pickle
def write_pickle(corpus:Corpus, dest:str):
    with open(dest,'ab') as pickle_file:
        pickle.dump(corpus, pickle_file)


# Définition d'une première fonction qui transforme les objets "Tokens" an un élément "etree" auquel sont rattachés les attributs "forme", "pos" et "lemme"
def analyse_to_xml(tokens: List[Token]) -> ET.Element:
    root = ET.Element("analyse")
    for tok in tokens:
        tok_element = ET.SubElement(root, "token")
        tok_element.attrib['forme'] = tok.forme
        tok_element.attrib['pos'] = tok.pos
        tok_element.attrib['lemme'] = tok.lemme
    return root


# Définition d'une deuxième fonction qui transforme les objets "Article" an un élément "etree" auquel sont rattachés les attributs "title" et "description". Pour chaque élément "article"
# créé, l'output de la fonction précédente "analyse_to_xml" est rajouté en obtenant la structure suivante:

# Article = ["titre","description","output de analyse_to_xml"]
def article_to_xml(article: Article) -> ET.Element:
    art = ET.Element("article")
    title = ET.SubElement(art, "title")
    description = ET.SubElement(art, "description")
    title.text = article.titre
    description.text = article.description
    art.append(analyse_to_xml(article.analyse))
    return art


# Définition d'une dernière fonction concernant la transformation en objet xml. Un élément etree "Corpus" est ajouté en tant que racine du document xml, ensuite trois attributs lui sont
# rattachés : "begin", "end" et "cat" pour "catégories". Un sous élément "content" est ensuite ajouté et il contiendra l'output de la fonction précédente "article_to_xml".
def write_xml(corpus: Corpus, destination: str):
    root = ET.Element("corpus")
    root.attrib['begin'] = corpus.begin
    root.attrib['end'] = corpus.end
    cat = None
    if len(corpus.categorie) > 1:
        cat = ", ".join(corpus.categorie)
    else:
        cat = corpus.categorie[0]
    root.attrib["categories"] = cat
    content = ET.SubElement(root, "content")
    for article in corpus.articles:
        art_xml = article_to_xml(article)
        content.append(art_xml)
    tree = ET.ElementTree(root)
    ET.indent(tree)
    tree.write(destination)
