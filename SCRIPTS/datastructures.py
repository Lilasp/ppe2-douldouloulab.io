# --------------------------------------------------------------------------------------------------------------------------------------------------------------

# Dans ce script sont définies trois classes d'objets : Token, Article et Corpus. La hierarchie de ces objets est la suivante : 

# [Corpus[Article[Token]]]

# Le but est d'obtenir un objet Corpus qui contient à l'intérieur les textes des articles du corpus Le Monde et les informations concernant ses tokens.

# --------------------------------------------------------------------------------------------------------------------------------------------------------------


# Importations des librairies
from typing import List
from dataclasses import dataclass
from pathlib import Path

# Création classe Token
@dataclass
class Token:
    forme:str
    lemme:str
    pos:str


# Création classe Article (contient l'objet Token)
@dataclass
class Article:
    titre: str
    description: str
    analyse: List[Token]


# Création classe Corpus (contient l'objet Article)
@dataclass
class Corpus:
    categorie: List[str]
    begin: str
    end: str
    chemin: Path
    articles: List[Article]

