r"""
LDA Model
=========

Introduces Gensim's LDA model and demonstrates its use on the NIPS corpus.

Cette fonction fait du topic modeling sur les corpus sortis grâce à extract_many_base.
On peut choisir les différentes options grâce à argparse, et on peut visualiser les résultats.

"""

# Importation des modules nécessaires
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
from gensim.models import Phrases
from gensim.corpora import Dictionary
from gensim.models import LdaModel
#permet la visualisation
import pyLDAvis
import pyLDAvis.gensim_models as gensimvis
from pprint import pprint
from xml.etree import ElementTree as ET
import argparse
import sys
import glob
import pandas as pd
import json


# Cette fonction prend en input un corpus xml et créé un docs (nécessaire au LDA) selon les POS donnés en arguments.
def from_xml_to_lemma_list(output, word_or_lemma, pos_filtre):
    #Comme vient chercher la liste des lemmes déjà parsé dans nos fichier xml
    for fichier in glob.glob(output+'/*.xml'):
        with open(fichier, "r") as f:
            xml = ET.parse(f)
            docs=[]
            for article in xml.findall(".//analyse"):
                doc = []
                for token in article.findall("./token"):
                    if pos_filtre != "":
                        if token.attrib['pos'] in pos_filtre:
                            doc.append(token.attrib[word_or_lemma]+"/"+token.attrib['pos'])
                    else:
                        doc.append(token.attrib[word_or_lemma]+"/"+token.attrib['pos'])

                if len(doc) > 0:
                    docs.append(doc)
    return docs


# Cette fonction prend en input un corpus json et créé un docs (nécessaire au LDA) selon les POS donnés en arguments.
def from_json_to_lemma_list(output, word_or_lemma, pos_filtre):
    for dico in glob.glob(output+'/*.json'):
        with open(dico, "r") as json_file:
            docs=[]
            jsonObj = json.load(json_file)
            articles = jsonObj["articles"]
            for article in articles:
                doc = []
                for analyse in article['analyse']:
                    if pos_filtre != "":
                        if analyse['pos'] in pos_filtre:
                            doc.append(f"{analyse[word_or_lemma]}/{analyse['pos']}")
                    else:
                        doc.append(f"{analyse[word_or_lemma]}/{analyse['pos']}")
                if len(doc) > 0:
                    docs.append(doc)
    return docs


# Cette fonction prend en input un corpus pickle et créé un docs (nécessaire au LDA) selon les POS donnés en arguments.
def from_pickle_to_lemma_list(output, word_or_lemma, pos_filtre):
    for fichier in glob.glob(output+'/*.pickle'):
        pickleObj = pd.read_pickle(fichier)
        docs=[]
        articles = [art for art in pickleObj.articles]
        for article in articles:
            doc = []
            #print(article)
            for token in article.analyse:
                #print(token)
                if pos_filtre != "":
                    if token.pos in pos_filtre:
                        if word_or_lemma == "lemme":
                            doc.append(f"{token.lemme}/{token.pos}")
                        elif word_or_lemma == "forme":
                            doc.append(f"{token.forme}/{token.pos}")

                else:
                    for part_of_speech in pos_filtre:
                        if token.pos == part_of_speech:
                            if word_or_lemma == "lemme":
                                doc.append(f"{token.lemme}/{token.pos}")
                            elif word_or_lemma == "forme":
                                doc.append(f"{token.forme}/{token.pos}")
                        else:
                            continue
                        

            if len(doc) > 0:
                docs.append(doc)
    return docs

# Fonction pour ajouter des bigrammes pour l'enrichissement du modèle
def add_bigrams(docs, min_count=20):
    bigram = Phrases(docs, min_count=20)
    for idx in range(len(docs)):
        for token in bigram[docs[idx]]:
            if '_' in token:
                # Token is a bigram, add to document.
                docs[idx].append(token)
    return docs


# construction du modèle lda selon les arguments donnés en ligne de commande
def build_lda_model(docs, min, max, n_topics, iteration):

    # Create a dictionary representation of the documents.
    dictionary = Dictionary(docs)

    # Filter out words that occur less than 20 documents, or more than 50% of the documents.
    dictionary.filter_extremes(no_below=int(min), no_above=float(max)) 

    # Bag-of-words representation of the documents.
    corpus = [dictionary.doc2bow(doc) for doc in docs]


    # Set training parameters.
    num_topics = int(n_topics)
    chunksize = 2000
    passes = 20
    iterations = int(iteration)
    eval_every = None  # Don't evaluate model perplexity, takes too much time.

    # Make an index to word dictionary.
    temp = dictionary[0]  # This is only to "load" the dictionary.
    id2word = dictionary.id2token

    model = LdaModel(
        corpus=corpus,
        id2word=id2word,
        chunksize=chunksize,
        alpha='auto',
        eta='auto',
        iterations=iterations,
        num_topics=num_topics,
        passes=passes,
        eval_every=eval_every
    )
    return corpus, dictionary, model


def print_coherence(model, corpus):
    top_topics = model.top_topics(corpus)

    # Average topic coherence is the sum of topic coherences of all topics, divided by the number of topics.
    avg_topic_coherence = sum([t[1] for t in top_topics]) / model.num_topics
    print('Average topic coherence: %.4f.' % avg_topic_coherence)

    print(top_topics)


#Fonction qui permet la visualisation
def save_html_viz(model, corpus, dictionary, output_path):
    vis_data = gensimvis.prepare(model, corpus, dictionary)
    with open(output_path, "w") as f:
        pyLDAvis.save_html(vis_data, f)



def main(min, max, num, it, pos, t, f, o, c, corpus_dir):
    if f == 'xml':
        docs = from_xml_to_lemma_list(corpus_dir, t, pos)
    elif f == 'json':
        docs = from_json_to_lemma_list(corpus_dir, t, pos)
    elif f == 'pickle':
        docs = from_pickle_to_lemma_list(corpus_dir, t, pos)
    else:
        print("méthode non disponible", file=sys.stderr)
        sys.exit()

    corpus, dico, model = build_lda_model(docs, min, max, num, it)
    if o is not None:
        save_html_viz(model, corpus, dico, o)
    if c:
        print_coherence(model, corpus)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-min", type=int, help="no_below variable. Filter out words that occur less than n documents", default=20)
    parser.add_argument("-max", type=float, help="no_above variable. 0<a<1. Filter out words that occur more than n pourcent of the document", default=0.5)
    parser.add_argument("-num", type=int, help="number of topics", default=10)
    parser.add_argument("-it", type=int, help="iterations number in the LDA model",  default=400)
    parser.add_argument("-pos", help="a list of pos separated by a comma, to filter the results. ex : NOUN,VERB", default="")
    parser.add_argument("-t", help="choose between form tokenization or lemma", default="lemme")
    parser.add_argument("-f", help="input format")
    parser.add_argument("-o", help="génère la visualisation ldaviz et la sauvegarde dans le fichier html indiqué", default=None)
    parser.add_argument("-c",  help="affiche les topics et leur cohérence", default=False)
    parser.add_argument("corpus_dir", help="root dir of the corpus data")
    args = parser.parse_args()
    main(args.min, args.max, args.num, args.it, args.pos, args.t, args.f, args.o, args.c, args.corpus_dir)

