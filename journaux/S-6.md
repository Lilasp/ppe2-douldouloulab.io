## SEMAINE 6 : 15/03/2023

# DEDOU

### EXO 1 : 
J'ai chioisi de travailler avec pathlib pour l'exercice 1 S6. Le script produit un dictionnaire avec un compteur des articles et leur titre + description. Le dictionnaire en output s'affiche en stdout lorsqu'on on appelle le script en ligne de commande avec en argument l'extrait xml fourni pour l'exercice.

### EXO 2 : 
J'ai choisi de travailler avec le module pathlib. Ce module permet d'accéder à tous les fichier XML du corpus de Le Monde qui nous a été fourni. Dans le script les fichiers xml sont parsés avec feedparser. Le traitement produit un fichier XML de plus de 500.000 lignes dont la balise <date></date> est renseignée graçe au parsing sur chaque XML du corpus et la balise <catégorie></catégorie> est renseignées graçe à une comparaison avec un dictionnaire conténant les noms des catégories équivalentes au code présent sur le nom de chaque fichier XML.
J'ai modifié le script ***extraire_deux.py***. Cette modification fait en sorte que dans le document XML de sortie apparaissent les titres et les descriptions de chaque articles du corpus triés par catégorie. Par rapport à l'ancienne version du script ***extraire_deux.py***, cette nouvelle version appelle la fonction que j'ai écrit lors de la l'éxercice 1 (S6). De plus, le module argparse est utilisé pour faire en sorte que l'utilisateur séléctionne le corpus et la catégorie sur laquelle les articles seront triés.

#### Difficultés : 
Il a été très difficile a comprendre le role qui aurait du jouer le script extraire_un.py à appeler dans le nouveau script de l'exo2. extraire_un.py affiche les titres et la description de chaque article, alors que dans l'exo2 il est demandé d'extraire la date et la catégorie. 


## MATHOU	

### EXO 1 :

J'ai choisi de travailler avec Etree pour parser le document xml et renvoyer un dictionnaire avec les titres et des descriptions des articles.\
Chaque clés du dictionnaire est "Titre n" ou "Description n", et chaque valeur est le texte du titre de l'article n ou bien le texte de la description de l'article n.

### EXO 2 :

Nous avons travaillé avec pathlib et argparse. J'ai fait la partie concernant le tri des articles par date.\
Afin de faire tourner ce script, il faut appeler le script suivi du chemin vers le corpus "2022", suivi d'une date au format MMDD (4 integers).\
La fonction de l'exo 1 est appalé dans l'exo 2, ce qui renvoie le dictionnaire, et l'output est un fichier xml avec une balise <date> au début puis les titres et les descriptions de tous les articles trouvés à cette date.

#### Essai de BaseX


J'ai ensuite téléchargé BaseX et importé la database 2022 dedans. Après de longues heures à me renseigner sur XQuery et BaseX, je n'ai toujours pas percé l'arborescence de cette database.\
J'ai compris qu'il s'agissait d'une layout de database files triés par nature (inf, atv, atvl, atvr, tbl, tbli, ...), mais je n'ai pas réussi à cheminer dans l'arborescence jusqu'aux balises qui m'intéressaient.\
En effet, quelqeu chose comme : "for $e in collection("2022")//title
return $e" ne renvoie rien.\
J'ai ensuite essayé de m'entraîner sur le fichier extrait.xml qu'on avait utilisé pour l'exercice 1. 

# Lilou

### EXO 1 : 
J'ai travaillé avec feedparser et nous avons finalement préféré le script de Mathilde, plus complet.

### EXO 2 : 
J'ai choisi de travailler avec le module os.



### TODO :

- Fusionner les deux scripts (tri par catégorie + par date) (difficile)
- Trouver comment rendre la présence d'un argument optionnel avec argparse (facile)


<br/>

## Semaine 7 : FIN DU MONDE


