## Semaine 1 : 01/02/23

Nous gardons le même groupe : Lilas Pastré, Diego Rossini et Mathilde Charlet.
Nous avons découvert gitLab et fait des exercices sur git.

<br>
<br>

## Semaine 2 : 08/02/23

Création de notre repository sur gitLab : PPE2-Douldoulou.

### Exo 1:
- Création de branche individuelle et d'un corpus commun.
- **Exo fini**, tag posé

### Exo 2:
Pas fait

### Exo3:
- Création de la branche **page** avec *git checkout -b page*
- Création du **journal de bord** sur la branche **page** avec *touch jdb.md*



***Recap de la séance***:
- ***git status*** = commande la plus importante
- La branche sur laquelle on se trouve est importante si on veut recréer une branche!
- On peut renommer les branches avec *git branch -m*
- On va créer au moins **une branche chacun par semaine**, en suivant cette nomenclature : MC-s1 ou MC-p-s1
- A chaque fin d'exo, on doit tagger le commit.
- On a appris a merger : *git checkout mathou - git merge dedo/lilou - git checkout main - git merge mathou*
<br>

*Aller voir gitk!*


### Exo 2:
- Création des scripts *extraire_lexique.py* r1, r2 et r3 pour extraire les mots des articles, compter le nombre d'occurrences et le nombre de fichiers où le mot apparaît
- Fusion des scripts r1 et r2, difficultés pour merger avec d'autres branches
- Création d'un autre script *extraire_lexique.py* qui utilise les fonctions de r1, r2 et r3 pour créer le lexique

***Recap de la séance***: <br>
Nous n'avons pas fini l'exercice 2 car nous avons rencontré des problèmes pour merger les branches, et donc les scripts qui y étaient. De plus, nous n'avons pas pushé nos tags.

<br>
<br>

## Semaine 3 : 15/02/23


### Exo 2 TP1:
- On a repris les scripts et on a réussi à les fusionner dans extraire_lexique.py, qu'on a fusionné dans main avec le tag "*s1ex2fin*". **Exo fini!**

### Exo 3 TP1: 
- Nous avons mis à jour le journal de bord à partir de nos branches individuelles. **Exo fini!**

***Recap de la séance***: <br>
*Nous avons fini le TP1, youpi !*


### Exo1 TP2:

#### DEDO: r1

- Ecriture de la fonction r1 "fichiers_list_str_TP2"
- Ajout du tag DR-s3-e1 à la branche DR-s2
<br>

**RMQ :** 
La consigne était difficile à comprendre : "comme une liste des fichiers en argument" ne permet pas de comprendre s'il est nécessaire d'afficher le contenu des articles du corpus dans le terminal ou non. 

<br>

#### LILOU : r2


<br>

#### MATHOU: r3

- Après un petit passage du côté obscur de la lune, rattrapage du cours raté cette semaine, lecture de la doc de sys.stdin, sys.argv et argparse. Les slides ne m'ont pas beaucoup aidé.
- Je ne comprends pas où utiliser argparse dans le tp2.
- Écriture de la fonction r3 lire_stdin_liste_tp2_r3().
- Difficulté pour utiliser fileinput.isstdin() dans un "if" statement pour appeler les fonctions dans la main() -> "raise RuntimeError("no active input()")".
 Je comprends que l'objet input() n'est pas encore créer, d'où l'erreur, mais comme il se créé à partir des arguments passés en entrée standard, je ne comprends donc pas l'utilité de la méthode .isstdin() (qui, pour moi, devait vérifier de façon booléenne s'il y a ou non des arguments passés en l'entrée standard).
- Solution : j'ai utilisé sys.argv[1:] à la place, qui permet de faire une différence entre le script appelé avec les arguments donnés après où bien avec les arguments donnés en entrée standard.
- Ajout du tag MC-s3-e1 à la branche MC-s2.
<br> 

**RMQ GIT :**
- MERGE: il faut merger à partir de notre branche : (en étant sur MC-s2) git merge DR-s2/LP-s2 -> résoudre conflits -> ACP (add, commit, push)
- TAG: NE PAS OUBLIER DE PUSHER LES TAGS!

<br>
<br>


**Echec écriture fonction r2**

Nous n'avons pas reussi à écrire la fonction r2. Nous avons tous les trois essayé d'écrire la fonction r2 mais nous avons rencontré la meme difficulté : l'entrée standard "cat Corpus/*txt" affiche le contenu du corpus en respectant les retours à la ligne, ce qui oblige à l'écriture de quelques lignes de code Python qui puissent afficher en sortie le contenu de chacun des articles sur une seule ligne, comme domandé dans l'exercice  1 TP2.

Difficultés rencontrées :

- D'abord nous avons essayé d'enlver les retours à la ligne avec rstrip() et replace() appliqué à l'entrée standard enregistré sous sys.stdin. Toutefois nous avons obtenu comme output tous les lignes des textes du corpus les une après les autres sans séparation entre les différents articles;
- Ensuite nous avons essayé de trouver une méthode/fonction de la librairie sys qui permet d'afficher les options/arguments passé en entrée standard avant l'appel du script "extraire_lexique.py" sur la ligne de commande. Le but était celui d'isoler, d'un coté, l'option "cat", e de l'autre, l'argument "Corpus/*.txt". Cela nous aurais permis d'avoir accès au corpus et le modifier (une ligne = un article) sans avoir à l'écrire en dur, pour ensuite appeler "cat" sur le corpus modifié. Toutefois nous n'avoir pas trouvé le moyen de realiser cela;
- Par la suite nous avons essayé sts.argv mais cette fonction ne prend pas en compte les options/arguments passé en entrée standard avant que l'on appelle le script python. Donc nous avons essayé d'utiliser la librairie argparse et son parseur mais encore une fois nous n'avons pas su comment manipuler les options/arguments passés avant sur bash avant l'appel du script python.
- Une autre idée était celle de manipuler ligne par ligne via sys.stdin.readlines(), mais nous n'avons pas trouvé un élément qui aurait pu nous indiquer à quel moment un article se termine et en commence un autre. Nous avons donc hésité à pre-traiter notre corpus de manière défnitive, mais cela aurait signifé l'écriture en dur du chemin vers le coprus (cela n'est pas ce qui est demandé dans l'exrcice);
- Nous avons aussi reflechi à écrire une fonction r2 dans laquelle, à l'intérieur, on appelle r1 pour avoir accès au corpus. Mais cela signifie écrire au moins deux lignes de commande sur bash (cela autait peu de sens et crée une interdépendance qui semble bien en contraste avec l'objectif du TP2).

Un autre difficulté, liée encore à l'écriture de la fonction r2, a été celle d'écrire la fonction qui combine r1,r2,r3. Etant donné que r1 appelle directement le script "extraire_lexique.py" avec un argument "Corpus/*.txt" et que r2 et r3 ont une entrée standard avant l'appel du script, nous avons essayé d'écrire une fonction avec un if statement. Si il y a un ou plusieurs arguments présents après le script, La fonction r1 est appelée. « Else », on essaie d’appeler r2 puis r3. 
Toutefois, nous n'avons pas trouvé comment distinguer l'appel de r2 et l'appel de r3, étant donné que toutes les deux ont une stdin et que nous n'arrivons pas à les lire selon leur différentes commandes bash. Un try/except nous permet de prendre en compte le cas où ni entrée standard ni arguments après le script ne sont passés.


## Semaine 4 : 22/02/23


<br>
<br>

## Semaine 5 : 01/03/23

