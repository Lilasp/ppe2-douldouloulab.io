## SEMAINE 8 : 29/03/2023

## MATHOU

### EXO 2 :

J'ai choisi de travailler avec le format JSON. Nous avons d'abord repris le code de correction, pour tous repartir sur un code fonctionnel.\
J'ai écrit un if statement dans la fonction qui appelle le deuxième script, celui qui "yield" les titres et descr, afin de voir si il y a un output donné en argument et si celui-ci se termine par .json.\
Si c'est le cas, je créé un dico avec les titres et descr, que je transforme en string json grace a json.dumps() puis j'ouvre un fichier et j'écris ce string dedans.



## DEDOU

### Exo 2 :

J'ai choisi de transformer la sortie du script extract_manu_base.py en formet pickle. La sortie va créer un fichier pickle (encodage binaire) avec un dictionnaire python conténant les titres et les description des articles selon la/les catégorie(s) choisie(s).
La plus grande difficulté a été celle de comprendre qu'il fallait marquer 'ap' (append binary) dans les paramètres de la ligne de script "with open pickle file".
